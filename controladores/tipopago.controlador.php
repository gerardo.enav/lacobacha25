<?php

class ControladorTipoPago{

	/*=============================================
	CREAR CATEGORIAS
	=============================================*/

	static public function ctrCrearTipoPago(){

		if(isset($_POST["nuevaTipoPago"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaTipoPago"])){

				$tabla = "tipopago";

				$datos = $_POST["nuevaTipoPago"];

				$respuesta = ModeloTipoPago::mdlIngresarTipoPago($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido guardada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipopago";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "tipopago";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR CATEGORIAS
	=============================================*/

	static public function ctrMostrarTipoPago($item, $valor){

		$tabla = "tipopago";

		$respuesta = ModeloTipoPago::mdlMostrarTipoPago($tabla, $item, $valor);

		return $respuesta;
	
	}

	/*=============================================
	EDITAR CATEGORIA
	=============================================*/

	static public function ctrEditarTipoPago(){

		if(isset($_POST["editarTipoPago"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarTipoPago"])){

				$tabla = "tipopago";

				$datos = array("categoria"=>$_POST["editarTipoPago"],
							   "id"=>$_POST["idTipoPago"]);

				$respuesta = ModeloTipoPago::mdlEditarTipoPago($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipopago";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "tipopago";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	BORRAR CATEGORIA
	=============================================*/

	static public function ctrBorrarTipoPago(){

		if(isset($_GET["idTipoPago"])){

			$tabla ="TipoPago";
			$datos = $_GET["idTipoPago"];

			$respuesta = ModeloTipoPago::mdlBorrarTipoPago($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipopago";

									}
								})

					</script>';
			}
		}
		
	}
}

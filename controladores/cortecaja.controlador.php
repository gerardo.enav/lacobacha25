<?php


class ControladorCorte{

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/

	static public function ctrMostrarCortes($item, $valor){

		$tabla = "cortecaja";

		$respuesta = ModeloCorte::mdlMostrarCortes($tabla, $item, $valor);

		return $respuesta;

	}


	static public function ctrCortesMostrar($item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		$tabla = "cortecaja";

		$respuesta = ModeloCorte::mdlCortesMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5);

		return $respuesta;
		
	}



	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function ctrRangoFechasCorte($fechaInicial, $fechaFinal){

		$tabla = "cortecaja";

		$respuesta = ModeloCorte::mdlRangoFechasCorte($tabla, $fechaInicial, $fechaFinal);

		return $respuesta;
		
	}


	/*=============================================
	CREAR VENTA
	=============================================*/

	static public function ctrCrearCorte(){

		if(isset($_POST["nuevoCorte"])){

			/*=============================================
			ACTUALIZAMOS USUARIO
			=============================================*/	

			$item = "id";
			$valor = $_SESSION["id"];
			$item1b = "ultimo_movimiento";
			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			$valor1b = $fecha.' '.$hora;

			$fechaUsuario = ControladorUsuarios::ctrActualizarUsuario($item1b, $valor1b, $item, $valor);

			/*=============================================
			GUARDAR CORTE
			=============================================*/	

			$tabla = "cortecaja";

			$datos = array("usuario_id"=>$valor,
						   "codigo"=>$_POST["nuevoCorte"],
						   "total"=>$_POST["nuevoTotalCorte"],
						   "fechaalta"=>$valor1b,
						   "sucursal_id"=>$_SESSION["sucursal_id"]);

			$ingresa = ModeloCorte::mdlIngresarCorte($tabla, $datos);
			// echo $respuesta;
				$item2 = "usuario_id";
				$max = ModeloCorte::mdlMaxIDCorte($tabla, "usuario_id", $valor);
				// echo "NUM: ".$max['idmax']; exit();

				$nuevocorte = ModeloCorte::mdlMostrarCortes($tabla, $item, $max['idmax'], null, null, null, null, null, null, null, null);

				$tablaVentas = "ventas";

				if ($_SESSION["tipousuario_id"]==1) {
					$respuesta = ControladorVentas::ctrActualizarVenta("corte_id", $max['idmax'], "corte_id", "0", null, null,NULL,NULL,NULL,NULL,NULL,NULL);
				}else{
					$respuesta = ControladorVentas::ctrActualizarVenta("corte_id", $max['idmax'], "corte_id", "0", "usuario_id", $valor,"sucursal_id",$_SESSION["sucursal_id"],NULL,NULL,NULL,NULL);
				}


			if($respuesta == "ok"){
				
				echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "success",
					  title: "El corte '.$nuevocorte['codigo'].' ha sido guardado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "cortecaja";

								}
							})

				</script>';

			}else{
								echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "error",
					  title: "El corte no ha podido generarse",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "#";

								}
							})

				</script>';
			}

		}

	}

static public function ctrMaxIDCorte($item, $valor){

		$tabla = "cortecaja";

		$respuesta = ModeloCorte::mdlMaxIDCorte($tabla, $item, $valor);

		return $respuesta;

}
}
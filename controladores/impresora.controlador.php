/*
	Vamos a imprimir un logotipo
	opcional. Recuerda que esto
	no funcionará en todas las
	impresoras
 
	Pequeña nota: Es recomendable que la imagen no sea
	transparente (aunque sea png hay que quitar el canal alfa)
	y que tenga una resolución baja. En mi caso
	la imagen que uso es de 250 x 250
*/
 
# Vamos a alinear al centro lo próximo que imprimamos
$printer->setJustification(Printer::JUSTIFY_CENTER);
//$printer->text(":::::REIMPRESION:::::" . "\n");

# Vamos a alinear al centro lo próximo que imprimamos
$printer->setJustification(Printer::JUSTIFY_CENTER);
 
/*
	Intentaremos cargar e imprimir
	el logo
*/
try{
	$logo = EscposImage::load("logo.png", false);
    $printer->bitImage($logo);
}catch(Exception $e){/*No hacemos nada si hay error*/}
 
/*
	Ahora vamos a imprimir un encabezado
*/
 
$printer->text("COCINA ECONOMICA" . "\n");
$printer->text("Con Próposito" . "\n");
// $printer->text("SAN JOSE DEL BARRO #103" . "\n");
// $printer->text("SANTO TOMAS II" . "\n");
$printer->text($sucursal->calle." #".$sucursal->numext);
if (!isset($sucursal->$numint)) {
	$printer->text(" Int. ".$sucursal->numint."\n");
}else{
	$printer->text("\n");
}
$printer->text($sucursal->colonia . "\n");
$printer->text("C.P. ".$sucursal->cp." ".$sucursal->ciudad. "\n");
$printer->text($sucursal->telefono. "\n");
#La fecha también

$printer->text(date("d-m-Y H:i") . "\n\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->text("CANT...DESCRIPCION.......PRECIO\n"); 
$printer->text("------------------------------" . "\n"); 
 
/*
	Ahora vamos a imprimir los
	productos
*/
 
 /*
Se extraen los productos de la consulta de la venta para imprimirlos en el ticket
*/
foreach($operations as $operation){
	$product  = $operation->getProduct();
 
	/*Alinear a la izquierda para la cantidad y el nombre*/
	$printer->setJustification(Printer::JUSTIFY_LEFT);
    $printer->text("  ".$operation->q . "   " . $product->name . "\n");
 
    /*Y a la derecha para el importe*/
    $printer->setJustification(Printer::JUSTIFY_RIGHT);
    $printer->text(' $' . $operation->q*$product->price_out . "\n");
    $total+=$operation->q*$product->price_out;
}

 
/*
	Terminamos de imprimir
	los productos, ahora va el total
*/
$printer->text("--------\n");
$printer->text("TOTAL: $". number_format($total,2) ."\n");
$printer->text("SU PAGO: $". number_format($_POST["cantidadpagar"],2) ."\n");
if ($_POST["cambioventa"]>0) {
	$printer->text("SU CAMBIO: $". number_format($_POST["cambioventa"],2) ."\n");
}

# Vamos a alinear al centro lo próximo que imprimamos
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->text("Forma de Pago: \n".$paytype->name);
 
/*
	Podemos poner también un pie de página
*/
$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->text("\n\nGracias por su compra\n¡BUEN PROVECHO!\n\n");

$printer->text("ID Venta: ".$_POST["id"]."\n");
$printer->text("LE ATENDIO: ".$cajero->name." ".$cajero->lastname."\n");
$printer->text("LUNES A SABADO DE ".strftime("%H:%M", $horaapertura)."-".strftime("%H:%M", $horacierre));

$printer->text("\n¡SIGUENOS EN FACEBOOK!\n");

/*Alimentamos el papel 3 veces*/
$printer->feed(3);
 
/*
	Cortamos el papel. Si nuestra impresora
	no tiene soporte para ello, no generará
	ningún error
*/
$printer->cut();
 
/*
	Por medio de la impresora mandamos un pulso.
	Esto es útil cuando la tenemos conectada
	por ejemplo a un cajón
*/
$printer->pulse();
 
/*
	Para imprimir realmente, tenemos que "cerrar"
	la conexión con la impresora. Recuerda incluir esto al final de todos los archivos
*/
$printer->close();



				// $impresora = "epson20";

				// $conector = new WindowsPrintConnector($impresora);

				// $imprimir = new Printer($conector);

				// $imprimir -> text("Hola Mundo"."\n");

				// $imprimir -> cut();

				// $imprimir -> close();

				// $impresora = "Cocecoweb";

				// $conector = new WindowsPrintConnector($impresora);

				// $printer = new Printer($conector);

				// $printer -> setJustification(Printer::JUSTIFY_CENTER);

				// $printer -> text(date("Y-m-d H:i:s")."\n");//Fecha de la factura

				// $printer -> feed(1); //Alimentamos el papel 1 vez*/

				// $printer -> text("Inventory System"."\n");//Nombre de la empresa

				// $printer -> text("NIT: 71.759.963-9"."\n");//Nit de la empresa

				// $printer -> text("Dirección: Calle 44B 92-11"."\n");//Dirección de la empresa

				// $printer -> text("Teléfono: 300 786 52 49"."\n");//Teléfono de la empresa

				// $printer -> text("FACTURA N.".$_POST["nuevaVenta"]."\n");//Número de factura

				// $printer -> feed(1); //Alimentamos el papel 1 vez*/

				// $printer -> text("Cliente: ".$traerCliente["nombre"]."\n");//Nombre del cliente

				// $tablaVendedor = "usuarios";
				// $item = "id";
				// $valor = $_POST["idVendedor"];

				// $traerVendedor = ModeloUsuarios::mdlMostrarUsuarios($tablaVendedor, $item, $valor);

				// $printer -> text("Vendedor: ".$traerVendedor["nombre"]."\n");//Nombre del vendedor

				// $printer -> feed(1); //Alimentamos el papel 1 vez*/

				// foreach ($listaProductos as $key => $value) {

				// 	$printer->setJustification(Printer::JUSTIFY_LEFT);

				// 	$printer->text($value["descripcion"]."\n");//Nombre del producto

				// 	$printer->setJustification(Printer::JUSTIFY_RIGHT);

				// 	$printer->text("$ ".number_format($value["precio"],2)." Und x ".$value["cantidad"]." = $ ".number_format($value["total"],2)."\n");

				// }

				// $printer -> feed(1); //Alimentamos el papel 1 vez*/			
				
				// $printer->text("NETO: $ ".number_format($_POST["nuevoPrecioNeto"],2)."\n"); //ahora va el neto

				// $printer->text("IMPUESTO: $ ".number_format($_POST["nuevoPrecioImpuesto"],2)."\n"); //ahora va el impuesto

				// $printer->text("--------\n");

				// $printer->text("TOTAL: $ ".number_format($_POST["totalVenta"],2)."\n"); //ahora va el total

				// $printer -> feed(1); //Alimentamos el papel 1 vez*/	

				// $printer->text("Muchas gracias por su compra"); //Podemos poner también un pie de página

				// $printer -> feed(3); //Alimentamos el papel 3 veces*/

				// $printer -> cut(); //Cortamos el papel, si la impresora tiene la opción

				// $printer -> pulse(); //Por medio de la impresora mandamos un pulso, es útil cuando hay cajón moneder

				// $printer -> close();	
<?php
// setlocale(LC_ALL, 'es'); 
setlocale(LC_ALL,"es_MX.utf8");

date_default_timezone_set('America/Mexico_City');
// setlocale(LC_TIME, 'es_MX.UTF-8');

require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/categorias.controlador.php";
require_once "controladores/productos.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/ventas.controlador.php";
require_once "controladores/sucursales.controlador.php";
require_once "controladores/cortecaja.controlador.php";
require_once "controladores/pago.controlador.php";
require_once "controladores/tipopago.controlador.php";
require_once "controladores/estado.controlador.php";
require_once "controladores/municipio.controlador.php";
require_once "controladores/dias.controlador.php";

require_once "modelos/dias.modelo.php";
require_once "modelos/estado.modelo.php";
require_once "modelos/municipio.modelo.php";
require_once "modelos/tipopago.modelo.php";
require_once "modelos/usuarios.modelo.php";
require_once "modelos/categorias.modelo.php";
require_once "modelos/sucursales.modelo.php";
require_once "modelos/cortecaja.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/clientes.modelo.php";
require_once "modelos/ventas.modelo.php";
require_once "modelos/pago.modelo.php";
require_once "extensiones/vendor/autoload.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();
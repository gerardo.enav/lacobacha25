<?php

require_once "../controladores/sucursales.controlador.php";
require_once "../modelos/sucursales.modelo.php";

class AjaxSucursales{

	/*=============================================
	EDITAR CATEGORÍA
	=============================================*/	

	public $idSucursal;

	public function ajaxEditarSucursal(){

		$item = "id";
		$valor = $this->idSucursal;

		$respuesta = ControladorSucursales::ctrMostrarSucursales($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["idSucursal"])){

	$sucursales = new AjaxSucursales();
	$sucursales -> idSucursal = $_POST["idSucursal"];
	$sucursales -> ajaxEditarSucursal();
}

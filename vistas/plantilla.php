<?php

session_start();

// Set maximum execution time to 10 seconds this way
ini_set('max_execution_time', 0);
// or this way
set_time_limit(0);

//Tiempo en segundos para dar vida a la sesión.
$inactivo = 1200;//20min en este caso.

?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>La COBACHA 25</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" href="vistas/img/plantilla/icono-negro.ico">

   <!--=====================================
  PLUGINS DE CSS
  ======================================-->

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="vistas/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="vistas/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="vistas/dist/css/AdminLTE.css">
  
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="vistas/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

   <!-- DataTables -->
  <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="vistas/plugins/iCheck/all.css">

   <!-- Daterange picker -->
  <link rel="stylesheet" href="vistas/bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- Morris chart -->
  <link rel="stylesheet" href="vistas/bower_components/morris.js/morris.css">

  <!--=====================================
  PLUGINS DE JAVASCRIPT
  ======================================-->

  <!-- jQuery 3 -->
  <script src="vistas/bower_components/jquery/dist/jquery.min.js"></script>
  
  <!-- Bootstrap 3.3.7 -->
  <script src="vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- FastClick -->
  <script src="vistas/bower_components/fastclick/lib/fastclick.js"></script>
  
  <!-- AdminLTE App -->
  <script src="vistas/dist/js/adminlte.min.js"></script>

  <!-- DataTables -->
  <script src="vistas/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="vistas/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="vistas/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
  <script src="vistas/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>

  <!-- SweetAlert 2 -->
  <script src="vistas/plugins/sweetalert2/sweetalert2.all.js"></script>
   <!-- By default SweetAlert2 doesn't support IE. To enable IE 11 support, include Promise polyfill:-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

  <!-- iCheck 1.0.1 -->
  <script src="vistas/plugins/iCheck/icheck.min.js"></script>

  <!-- InputMask -->
  <script src="vistas/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="vistas/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="vistas/plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <!-- jQuery Number -->
  <script src="vistas/plugins/jqueryNumber/jquerynumber.min.js"></script>

  <!-- daterangepicker http://www.daterangepicker.com/-->
  <script src="vistas/bower_components/moment/min/moment.min.js"></script>
  <script src="vistas/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <!-- Morris.js charts http://morrisjs.github.io/morris.js/-->
  <script src="vistas/bower_components/raphael/raphael.min.js"></script>
  <script src="vistas/bower_components/morris.js/morris.min.js"></script>

  <!-- ChartJS http://www.chartjs.org/-->
  <script src="vistas/bower_components/Chart.js/Chart.js"></script>

  <!-- LIBRERIAS DE LA VERSION PASADA-->
<link rel="stylesheet" href="vistas/datapicker/datepicker.css" />
<script src="vistas/datapicker/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="vistas/datepair/jquery.timepicker.css" />
<script src="vistas/datepair/jquery.timepicker.js"></script>
<script src="vistas/datepair/datepair.js"></script>
<script src="vistas/datepair/jquery.datepair.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="vistas/chosen/chosen.css">
    <link rel="stylesheet" href="vistas/picker/jquery.inputpicker.css" />
    <script src="vistas/picker/jquery.inputpicker.js"></script>

<script type="text/javascript">

function cambiarSelect(){
    var seleccionados = $(".jornada input[type='checkbox']:checked");

    var valores = [];

    for(var i = 0; i < seleccionados.length; i++){
        valores.push($(seleccionados[i]).val());
    }

    $('.selectpicker').selectpicker('val', valores);
}

</script>


</head>

<!--=====================================
CUERPO DOCUMENTO
======================================-->

<body class="hold-transition skin-green sidebar-collapse sidebar-mini login-page">
 
  <?php

  if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){

   echo '<div class="wrapper">';

    /*=============================================
    CABEZOTE
    =============================================*/

    include "modulos/cabezote.php";

    /*=============================================
    MENU
    =============================================*/

    include "modulos/menu.php";

    /*=============================================
    CONTENIDO
    =============================================*/

    if(isset($_GET["ruta"])){

      if($_GET["ruta"] == "login" ||
         $_GET["ruta"] == "inicio" ||

         $_GET["ruta"] == "crear-corte" ||       
         $_GET["ruta"] == "ver-venta" ||
         $_GET["ruta"] == "crear-venta" ||
         $_GET["ruta"] == "editar-venta" ||
         $_GET["ruta"] == "reportes" ||
         $_GET["ruta"] == "salir"){

        include "modulos/".$_GET["ruta"].".php";

      } else if(
         $_GET["ruta"] == "almacen" ||
         $_GET["ruta"] == "ventas" ||
         $_GET["ruta"] == "productos" ||
         $_GET["ruta"] == "proveedores" ||
         $_GET["ruta"] == "compras" ||
         $_GET["ruta"] == "gastos" ||
         $_GET["ruta"] == "usuarios" ||
         $_GET["ruta"] == "cortecaja"){

        include "modulos/".$_GET["ruta"]."/index.php";

      }else if(
         $_GET["ruta"] == "crear" ||
         $_GET["ruta"] == "editar" ||
         $_GET["ruta"] == "detalle"){
// echo "subarchivo".$_SESSION['carpeta']."/".$_GET["ruta"].".php"; exit();
        include "modulos/".$_SESSION['carpeta']."/".$_GET["ruta"].".php";

      }else{

        include "modulos/404.php";

      }

    }else{

      include "modulos/inicio.php";

    }


    /*=============================================
    CONTROLADOR DE TIEMPO DE SESION
    =============================================*/

            //Comprobamos si esta definida la sesión 'tiempo'.
            // if(isset($_SESSION['tiempo']) ) {

                // //Tiempo en segundos para dar vida a la sesión.
                // $inactivo = 1200;//20min en este caso.

                //Calculamos tiempo de vida inactivo.
                $vida_session = time() - $_SESSION['tiempo'];

                    //Compraración para redirigir página, si la vida de sesión sea mayor a el tiempo insertado en inactivo.
                    if($vida_session > $inactivo)
                    {
                        //Removemos sesión.
                        session_unset();
                        //Destruimos sesión.
                        session_destroy();              
                        //Redirigimos pagina.

                          echo'<script>

                          swal({
                          type: "warning",
                          title: "La sesion ha caducado, por favor <B>Inicia</b> de nuevo.",
                          showConfirmButton: true,
                          confirmButtonText: "Cerrar"
                          }).then(function(result){
                          if (result.value) {

                          window.location = "login";

                          }
                          })

                          </script>';
                        // header("Location: ");

                        exit();
                    } else {  // si no ha caducado la sesion, actualizamos
                        $_SESSION['tiempo'] = time();
                    }


            // } else {
            //     //Activamos sesion tiempo.
            //     $_SESSION['tiempo'] = time();
            // }

    /*=============================================
    FOOTER
    =============================================*/

    include "modulos/footer.php";

    echo '</div>';

  }else{

    include "modulos/login.php";

  }

  ?>


<script src="vistas/js/plantilla.js"></script>
<script src="vistas/js/usuarios.js"></script>
<script src="vistas/js/categorias.js"></script>
<script src="vistas/js/productos.js"></script>
<script src="vistas/js/clientes.js"></script>
<script src="vistas/js/ventas.js"></script>
<script src="vistas/js/reportes.js"></script>
<script src="vistas/js/sucursales.js"></script>
<script src="vistas/js/cortecaja.js"></script>
<script src="vistas/js/pago.js"></script>

</body>
</html>

<?php


if($_SESSION["perfil"] == "Especial"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}


?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Ventas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar Ventas</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <a href="crear-venta">

          <button class="btn btn-primary">
            
          <i class="fa fa-plus-circle"></i>
          &nbsp;&nbsp;Agregar

          </button>

        </a>

         <button type="button" class="btn btn-default pull-right" id="daterange-btn">
           
            <span>
              <i class="fa fa-calendar"></i> 

              <?php

                if(isset($_GET["fechaInicial"])){

                  echo $_GET["fechaInicial"]." - ".$_GET["fechaFinal"];
                
                }else{
                 
                  echo 'Rango de fecha';

                }

              ?>
            </span>

            <i class="fa fa-caret-down"></i>

         </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
          <?php if($_SESSION["perfil"] != "Administrador"){ ?>
           <th style="width:10px"></th>
         <?php } ?>
           <th style="width:10px">#</th>
           <th>Código</th>
           <th>Fecha</th>
           <th>Sucursal</th>
           <th>Vendedor</th>
           <th>Total</th>
           <th style="width:10px;">Estatus</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          if(isset($_GET["fechaInicial"])){

            $fechaInicial = $_GET["fechaInicial"];
            $fechaFinal = $_GET["fechaFinal"];

          }else{

            $fechaInicial = date('yy-m-d');
            $fechaFinal = date('yy-m-d');

          }

          // echo $fechaInicial.$fechaFinal;

          $respuesta = ControladorVentas::ctrRangoFechasVentas($fechaInicial, $fechaFinal);

          foreach ($respuesta as $key => $value) {


            $respuestaUsuario = ControladorUsuarios::ctrMostrarUsuarios("id", $value["usuario_id"]);

            $sucursal = ControladorSucursal::ctrMostrarSucursales("id",$value["sucursal_id"], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

           echo '<tr>';

           if($_SESSION["perfil"] != "Administrador"){
           echo  '<td>
                      <button class="btn btn-default btnVerVenta" idVenta="'.$value["id"].'"><i class="fa fa-eye"></i></button>
                  </td>';
                }

          echo '<td>'.($key+1).'</td>

                  <td>'.$value["codigo"].'</td>
                  
                  <td>'.strftime("%A, %d de %B de %G, <B>%H:%M</B>", strtotime($value["fechaalta"])).'</td>

                  <td>'.$sucursal["nombre"].'</td>

                  <td>'.$respuestaUsuario["nombre"].'</td>

                  <td><B>$ '.number_format($value["total"],2).'</td>
                  
                  <td>';

                      if($value['estatusventa'] == 1) { 

                        echo "<p class='label label-success'>
                                <i class='glyphicon glyphicon-ok'></i> 
                                &nbsp;&nbsp;Finalizado
                              </p>"; 
                            }
                      else { 

                        echo "<p class='label label-warning'>
                               <i class='glyphicon glyphicon-time'></i> 
                               &nbsp;&nbsp;Pendiente
                              </p>"; 
                            }
            echo '</td>';


        if($_SESSION["perfil"] == "Administrador" && $value['corte_id'] == 0){

              echo '<td style="width:140px;">';
        }else{
          echo '<td style="width:15px;">';
        }

                    echo'<div class="btn-group">';

                      // <a class="btn btn-success" href="index.php?ruta=ventas&xml='.$value["codigo"].'">xml</a>
                        
                    // echo '<button class="btn btn-info btnImprimirFactura" codigoVenta="'.$value["codigo"].'">

                    //     <i class="fa fa-print"></i>

                    //   </button>';

                      echo '
                      <button class="btn btn-info btnVerVenta" idVenta="'.$value["id"].'"><i class="fa fa-eye"></i></button>';

                  if($_SESSION["perfil"] == "Administrador" && $value['corte_id'] == 0){


                      echo '<button class="btn btn-warning btnEditarVenta" idVenta="'.$value["id"].'"><i class="fa fa-pencil"></i></button>';

                      echo '<button class="btn btn-danger btnEliminarVenta" idVenta="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                    }

                    echo '</div>';

                 echo ' </td>';
                echo '</tr>';
            }

        ?>
               
        </tbody>

       </table>

       <?php

      $eliminarVenta = new ControladorVentas();
      $eliminarVenta -> ctrEliminarVenta();

      ?>
       

      </div>

    </div>

  </section>

</div>




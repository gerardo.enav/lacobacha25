<?php

$item = null;
$valor = null;
$orden = "id";


$fechaInicial = date('yy-m-d');
$fechaFinal = date('yy-m-d');

$ventahoy = ControladorVentas::ctrSumaRangoFechasVentas($fechaInicial, $fechaFinal);
$ventas = ControladorVentas::ctrSumaTotalVentas();
$ventasaplicadas = ControladorVentas::ctrSumaVentas();

// $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);
// $totalCategorias = count($categorias);

$clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
$totalClientes = count($clientes);

$productos = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);
$totalProductos = count($productos);

?>



<div class="col-lg-3">

  <div class="small-box bg-aqua">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventahoy["total"],2); ?></h3>

      <p>Ganancias de Hoy</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasaplicadas["total"]; ?></h3>

      <p>Ventas de Hoy</p>

    </div>
    
    <div class="icon">
      
      <i class="ion ion-ios-sunny"></i>
    
    </div>

    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-green">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventas["total"],2); ?></h3>

      <p>Ganancias Cobradas</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasaplicadas["total"]; ?></h3>

      <p>Ventas Cobradas</p>

    </div>
    
        <div class="icon">
      
      <i class="ion-android-cart"></i>
    
    </div>
    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-yellow">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventas["total"],2); ?></h3>

      <p>Ganancias Pendientes</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasaplicadas["total"]; ?></h3>

      <p>Ventas Pendientes</p>

    </div>
    
    <div class="icon">
      
      <i class="ion-ios-clock"></i>
    
    </div>
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-purple">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventas["total"],2); ?></h3>

      <p>Ganancias Totales</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasaplicadas["total"]; ?></h3>

      <p>Ventas Generadas</p>

    </div>
    
    <div class="icon">
      
      <i class="ion ion-cash"></i>
    
    </div>
    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


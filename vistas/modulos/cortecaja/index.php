<?php

$_SESSION['carpeta'] = $_GET["ruta"];

if($_SESSION["perfil"] == "Especial"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}


if ($_SESSION["tipousuario_id"] == 1) {
  // echo "AQUI".$_SESSION["tipousuario_id"]; exit();
  $cortecaja = ControladorCorte::ctrMostrarCortes(null,null,null,null,null,null,null,null,null,null);
}else {
  // echo "3".$_SESSION['sucursal_id']; exit();
  $cortecaja = ControladorCorte::ctrCortesMostrar("usuario_id",$_SESSION['id'],"sucursal_id",$_SESSION['sucursal_id'],null,null,null,null,null,null);
}

?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Cortes de Caja
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Corte de Caja</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <a href="crear">

          <button class="btn btn-primary">
            
          <i class="fa fa-plus-circle"></i>
          &nbsp;&nbsp;Agregar

          </button>

        </a>

<!--          <button type="button" class="btn btn-default pull-right" id="daterange-btn">
           
            <span>
              <i class="fa fa-calendar"></i> 

              <?php

                if(isset($_GET["fechaInicial"])){

                  echo $_GET["fechaInicial"]." - ".$_GET["fechaFinal"];
                
                }else{
                 
                  echo 'Rango de fecha';

                }

              ?>
            </span>

            <i class="fa fa-caret-down"></i>

         </button> -->

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Código</th>
           <th>Cajero</th>
           <th>Sucursal</th>
           <th>Fecha Corte</th>
           <th>Total en Corte</th> 
           <th style="width:30px">Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          // if(isset($_GET["fechaInicial"])){

          //   $fechaInicial = $_GET["fechaInicial"];
          //   $fechaFinal = $_GET["fechaFinal"];

          // }else{

          //   $fechaInicial = null;
          //   $fechaFinal = null;

          // }




          foreach ($cortecaja as $key => $value) {

                  $vendedor = ControladorUsuarios::ctrMostrarUsuarios("id", $value["usuario_id"]);
                  $sucursal = ControladorSucursal::ctrMostrarSucursales("id",$value["sucursal_id"], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


           echo '<tr>

                  <td>'.($key+1).'</td>
                  <td>'.$value["id"].'</td>';

                  echo '<td>'.$vendedor["nombre"].'</td>

                  <td>'.$sucursal["nombre"].'</td>

                  <td>'.strftime("%A, %d de %B de %G, <B>%H:%M</B>", strtotime($value["fechaalta"])).'</td>

                  <td><B>$ '.number_format($value["total"],2).'</td>

                  <td>

                    <div class="btn-group">';

                      if($_SESSION["perfil"] != "Especial"){

                      echo '
                      <button class="btn btn-info btnDetalleCorte" idCorte="'.$value["id"].'"><i class="fa fa-eye"></i></button>';


                    }

                    echo '</div>  

                  </td>

                </tr>';
            }

        ?>
               
        </tbody>

       </table>
       

      </div>

    </div>

  </section>

</div>




<?php
if($_SESSION["perfil"] == "Especial" || $_GET["idCorte"] == 0){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

$totalCorte = 0;
$totalEfectivo = 0;
$totalTD = 0;
$totalTC = 0;
$totalVales = 0;

$corte = ControladorCorte::ctrMostrarCortes("id",$_GET["idCorte"],null,null,null,null,null,null,null,null);
$ventas = ControladorVentas::ctrVentasMostrar("corte_id", $_GET["idCorte"],null,null,null,null,null,null,null,null);
$sucursal = ControladorSucursal::ctrMostrarSucursales("id",$corte["sucursal_id"], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
// echo $sucursal["nombre"]; exit();


$cajero = ControladorUsuarios::ctrMostrarUsuarios("id", $corte["usuario_id"]);

  foreach ($ventas as $key => $value) {

    $totalCorte += $value['total'];
      if ($value['tipopago_id'] == 1) {
        $totalEfectivo += $value['total'];
      } else if ($value['tipopago_id'] == 2) {
        $totalTD += $value['total'];
      } else if ($value['tipopago_id'] == 3) {
        $totalTC += $value['total'];
      } else if ($value['tipopago_id'] == 4) {
        $totalVales += $value['total'];
      } 
  }

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Detalle Corte de Caja</h1>

    <ol class="breadcrumb">
      
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li><a href="cortecaja">Cortes de Caja</a></li>
      
      <li class="active">Detalle Corte</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="row">

      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-4 col-xs-12">
        
        <div class="box box-success">
          
            <div class="box-body">
  
<!--=====================================
DETALLE VENTA PARA PANTALLA GRANDE
======================================-->
    <div class="hidden-md hidden-sm hidden-xs">

      <table class="table">
        <thead>
          <tr>
            <th>Codigo Corte</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        
        <td>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i>
            </span>

            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $corte["id"]; ?>" readonly>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <!--==================================================-->

      <table class="table">
        <thead>
          <tr>
            <th>Fecha Corte</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
            </span>
            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo strftime("%A, %d de %B de %G, %H:%M", strtotime($corte["fechaalta"])); ?>" readonly>
          </div>
        </td>
        
      </tr>
    </tbody>
  </table>
  <!--==================================================-->
                    
  <table class="table">
        <thead>
          <tr>
            <th>Cajero</th>
          </tr>
        </thead>

        <tbody>
          <tr>
             <td>
    <div class="input-group">
      
      <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
    </span>

     <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $cajero["nombre"]; ?>" readonly>
 
    </div>
            </td>
             
          </tr>
        </tbody>
      </table>

  <!--==================================================-->

  <table class="table">
        <thead>
          <tr>
            <th>Sucursal</th>
          </tr>
        </thead>

        <tbody>
          <tr>
             <td>
    <div class="input-group">
      
      <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
    </span>

     <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $sucursal["nombre"]; ?>" readonly>
 
    </div>
            </td>
             
          </tr>
        </tbody>
      </table>
</div>
  <!--==================================================-->


<!--=====================================
DETALLE VENTA PARA PANTALLA MOVIL
======================================-->
    <div class="hidden-lg hidden-xl">
                  
      <table class="table">
        <thead>
          <tr>
            <th>Codigo Corte</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i>
            </span>

            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $corte["id"]; ?>" readonly>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <!--==================================================-->

      <table class="table">
        <thead>
          <tr>
            <th>Fecha Corte</th>
          </tr>
        </thead>
        <tbody>
      <tr>
        <td>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
            </span>
            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo strftime("%A, %d de %B de %G, %H:%M", strtotime($corte["fechaalta"])); ?>" readonly>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <!--==================================================-->                

  <!--==================================================-->

      <table class="table">
        <thead>
          <tr>
            <th>Cajero</th>
          </tr>
        </thead>
        <tbody>

          <tr>
             <td>
              <div class="input-group">
                
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
              </span>

               <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $cajero["nombre"]; ?>" readonly>
           
              </div>
            </td>
             
          </tr>
    </tbody>
  </table>
  <!--==================================================-->    
</div>
  <!--=====================================
  ENTRADA  DE TODOS LOS TOTALES
  ======================================-->
        <table class="table">

          <thead>

            <tr>
              <th></th>
 
            </tr>

          </thead>
          <tbody>
<?php if ($totalEfectivo >0){ ?>
            <tr>
              <td style="width: 30%">

                <div class="input-group pull-right">
               
                  <h6><b>Efectivo
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="$ <?php echo number_format($totalEfectivo,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
<?php } 

if ($totalTD >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Tarjeta Debito
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalTD,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
<?php } 

if ($totalTC >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Tarjeta Credito
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalTC,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
<?php } 

if ($totalVales >0){ ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Vales Despensa
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($totalVales,2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
<?php } ?>
            <tr>
              <td style="width: 30%">


                <div class="input-group pull-right">
               
                  <h6><b>Total
                </div>
              </td>

               <td style="width: 70%">
                
                <div class="input-group pull-right">
               
                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" value="<?php echo number_format($corte['total'],2); ?>" placeholder="00000" readonly required>
                  
                </div>

              </td>

            </tr>
          </tbody>

        </table>
  </div>
  </div>

</div>

<!--=====================================
LISTA DE PRODUCTOS DE LA VENTA
======================================-->

      <div class="col-lg-8 col-xs-12">
        
        <div class="box box-warning">

          <div class="box-header with-border"><h4><B>
            <i class="fa fa-list" aria-hidden="true"></i>
                &nbsp;Lista de Ventas</B></h4></div>

            <div class="box-body">
            
                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

          
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Código</th>
           <th>Vendedor</th>
           <th >Tipo Pago</th>
           <th>Fecha Venta</th> 
           <th>Total</th>

         </tr> 

        </thead>
                <?php

                foreach ($ventas as $key => $value) {
                  $vendedor = ControladorUsuarios::ctrMostrarUsuarios("id", $value['usuario_id']);

                  $tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$value["tipopago_id"]);
                  // $listaProducto = json_decode($venta["productos"], true);

                  echo '<tr>

                  <td>'.($key+1).'</td>
                  <td>'.$value["codigo"].'</td>';

                  echo '<td>'.$vendedor["nombre"].'</td>

                  <td>'.$tipopago["nombre"].'</td>


                  <td>'.strftime("%A, %d de %B de %G, %H:%M", strtotime($value["fechaalta"])).'</td>

                  <td><B>$'.number_format($value["total"],2).'</td>
                  </tr>
                  ';

         }?>
</table>

<!-- ==================================================================== -->

<?php if ($pago['comentarios']!=NULL){ ?>
<div class="hidden-lg hidden-xl">
  <table class="table">
    <thead>
      <tr>
        <th>Comentarios</th>
      </tr>
    </thead>

    <tbody>
      <tr>
         <td>
          <div class="input-group">
            <textarea name="comentarios-sm" class="form-control small-textarea large-textarea x-large-textarea medium-textarea" id="comentarios-sm" placeholder="Escribe tus comentarios adicionales sobre esta venta..." readonly style="width: 200%"></textarea>

          </div>
        </td>
      </tr>
    </tbody>
  </table>
 </div> 
  <!--==================================================-->

<div class="hidden-md hidden-sm hidden-xs">
  <table class="table">
    <thead>
      <tr>
        <th>Comentarios</th>
      </tr>
    </thead>

    <tbody>
      <tr>
         <td>
          <div class="input-group">
            <textarea name="comentarios-lg" class="form-control small-textarea large-textarea x-large-textarea medium-textarea" id="comentarios-lg" placeholder="Escribe tus comentarios adicionales sobre esta venta..." readonly=""></textarea>

          </div>
        </td>
      </tr>
    </tbody>
  </table>
 </div> 
<?php } ?>
  <!--==================================================-->

          </div>

        </div>

      </div>
   
  </section>


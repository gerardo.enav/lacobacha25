<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">

		<?php

		if($_SESSION["perfil"] == "Administrador"){

			echo '<li>

				<a href="inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>


			<li>

				<a href="compras">

					<i class="fa fa-shopping-basket" aria-hidden="true"></i>
					<span>Compras</span>

				</a>

			</li>';

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Especial"){

			echo '
			<li>

				<a href="productos">

					<i class="glyphicon glyphicon-cutlery"></i>
					<span>Productos</span>

				</a>

			</li>

			';

		}

		// if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

		// 	echo '<li>

		// 		<a href="clientes">

		// 			<i class="fa fa-users"></i>
		// 			<span>Clientes</span>

		// 		</a>

		// 	</li>';

		// }

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

			echo '<li>

				<a href="ventas">
					<i class="fa fa-shopping-cart"></i>
					<span>Ventas</span>
				</a>

			</li>
					<li>
						<a href="crear-venta">
							<i class="fa fa-cart-plus" aria-hidden="true"></i>
							<span>Nueva venta</span>
						</a>
					</li>';		

				echo '

			<li>
				<a href="cortecaja">
					<i class="fa fa-money"></i>
					<span>Corte de Caja</span>

				</a>
			</li>
			';

		if($_SESSION["perfil"] == "Administrador"){

			echo '
					<li>

						<a href="reportes">
							
							<i class="fa fa-line-chart"></i>
							<span>Reportes</span>

						</a>

					</li>';

		// echo   '<li>

		// 		<a href="clientes">

		// 			<i class="fa fa-users"></i>
		// 			<span>Colaboradores</span>

		// 		</a>

		// 	</li>';

	echo '<li>

				<a href="usuarios">

					<i class="fa fa-user"></i>
					<span>Usuarios</span>

				</a>

			</li>';

		}

				if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Especial"){

			echo '<li>

				<a href="almacen">

					<i class="fa fa-archive" aria-hidden="true"></i>
					<span>Almacen</span>

				</a>

			</li>

			<li>

				<a href="gastos">

					<i class="fa fa-list-alt" aria-hidden="true"></i>
					<span>Gastos</span>

				</a>

			</li>

			<li>

				<a href="proveedores">

					<i class="fa fa-address-card" aria-hidden="true"></i>
					<span>Proveedores</span>

				</a>

			</li>

			';

		}

		}

		?>

		</ul>

	 </section>

</aside>
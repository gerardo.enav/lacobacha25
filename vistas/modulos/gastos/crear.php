<?php

if($_SESSION["perfil"] == "Especial"){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

$item = null;
$tipopago = ControladorTipoPago::ctrMostrarTipoPago($item,$value);
?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Crear Venta </h1>

    <ol class="breadcrumb">
      
      <li>
        <a href="#">
          <i class="fa fa-dashboard"></i> Inicio
        </a>
      </li>

      <li>
        <a href="ventas">
          Ventas
        </a>
      </li>

      <li class="active">Crear Venta</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="row">

      <!--=====================================
      LA TABLA DE PRODUCTOS
      ======================================-->

      <div class="col-lg-7 col-xs-12">
        
        <div class="box box-warning">

          <div class="box-header with-border"></div>

          <div class="box-body">
            
            <table class="table table-bordered table-striped dt-responsive tablaVentas">
              
               <thead>
                 <tr>
                  <th >#</th>
                  <th >Código</th>
                  <th>Descripcion</th>
                  <th>Stock</th>
                  <th>Acciones</th>
                  <th>Imagen</th>
                </tr>
              </thead>

            </table>

          </div>

        </div>

      </div>

      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-5 col-xs-12">
        
        <div class="box box-success">
          
          <div class="box-header with-border"></div>

          <form role="form" method="post" class="formularioVenta">

            <div class="box-body">
  
              <div class="box">

                <!--=====================================
                ENTRADA DEL VENDEDOR
                ======================================-->
            
                <div class="form-group">
                
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control" id="nuevoVendedor" value="<?php echo $_SESSION["nombre"]; ?>" readonly>

                    <input type="hidden" name="idVendedor" value="<?php echo $_SESSION["id"]; ?>">

                  </div>

                </div> 

                <!--=====================================
                ENTRADA DEL CÓDIGO
                ======================================--> 

                <div class="form-group">
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>

                    <?php

                    $item = null;
                    $valor = null;

                    $ventas = ControladorVentas::ctrMostrarVentas($item, $valor);

                    if(!$ventas){

                      echo '<input type="text" class="form-control" id="nuevaVenta" name="nuevaVenta" value="1000000001" readonly>';

                    }else{

                      foreach ($ventas as $key => $value) {
                        
                      
                      }

                      $codigo = $value["codigo"] + 1;

                      echo '<input type="text" class="form-control" id="nuevaVenta" name="nuevaVenta" value="'.$codigo.'" readonly>';
                  
                    }

                    ?>
                    
                    
                  </div>
                
                </div>

                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

                <div class="form-group row nuevoProducto">

                </div>

                <input type="hidden" id="listaProductos" name="listaProductos">

                <div class="row" id="VentaDiv">

                  <!--=====================================
                  ENTRADA IMPUESTOS Y TOTAL
                  ======================================-->
                  
                  <div class="col-xs-8 pull-right">
                    
                    <table class="table">

                      <thead>

                        <tr>
                          <th>Total</th>      
                        </tr>

                      </thead>

                      <tbody>
                      
                        <tr>
                          
                          <td style="width: 10%">
                            
                            <div class="input-group"  style="display: none">
                           
                              <input  type="number" class="form-control input-lg" min="0" id="nuevoImpuestoVenta" name="nuevoImpuestoVenta" placeholder="0" required value="0">

                               <input type="hidden" name="nuevoPrecioImpuesto" id="nuevoPrecioImpuesto" required>

                               <input type="hidden" name="nuevoPrecioNeto" id="nuevoPrecioNeto" required>

                              <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                        
                            </div>

                          </td>

                           <td style="width: 90%">
                            
                            <div class="input-group">
                           
                              <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                              <input type="text" class="form-control input-lg" id="nuevoTotalVenta" name="nuevoTotalVenta" total="" placeholder="00000" readonly required>

                              <input type="hidden" name="totalVenta" id="totalVenta">
                              
                        
                            </div>
              <div class="box-body table-responsive" id="cambiovisualdiv">   

                  <div class="form-group" >
                    <label class="col-lg-6 control-label">Pago</label>
                    <div class="col-md-12 input-group">

                      <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>
                      <input type="text" id="pagovisual" class="form-control" required readonly>
                      <input type="hidden" id="pagoventa" name="pagoventa" class="form-control" required value="0" >
                    </div>
                  </div>

                  <div class="form-group" >
                    <label class="col-lg-6 control-label">Cambio</label>
                    <div class="col-md-12 input-group">

                      <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>
                      <input type="text" id="cambiovisual" class="form-control" required readonly>
                      <input type="hidden" id="cambioventa" name="cambioventa" class="form-control" required value="0" >
                    </div>
                  </div>

              </div>

                          </td>

                        </tr>

                      </tbody>

                    </table>

                  </div>

                </div>

    <table class="table">
      <thead>
        <tr>
          <th>Comentarios</th>
        </tr>
      </thead>

      <tbody>
        <tr>
           <td>
            <div class="input-group">
              <textarea name="comentarios-sm" class="form-control" id="comentarios-sm" name="comentarios-sm" placeholder="Escribe tus comentarios adicionales sobre esta venta..."></textarea>

            </div>
          </td>
        </tr>
      </tbody>
    </table>
 </div>
<div id="agregarPagoDiv">
        <button type="button" class="btn btn-default" id="agregarPago" data-toggle="collapse" 
      data-target="#mostrartipopago">
      <i class="glyphicon glyphicon-plus"></i>
      &nbsp;&nbsp;Agregar Pago
      </button>
</div>

 </div>

          <div class="box-body">

                <!--=====================================
                ENTRADA MÉTODO DE PAGO
                ======================================-->
 
                <div id="mostrartipopago"  class="form-group row collapse on">

                   <div class="col-xs-6">

                     <div class="input-group">

                      <select class="form-control" name="nuevoMetodoPago" id="nuevoMetodoPago">
                        <option value=""></option>
                                  <?php 
                                foreach ($tipopago as $key => $value) { 
                                  
                                  echo '<option value="'.$value['id'].'">'.$value['nombre'].'</option>';  } ?>
                      </select>

                    </div>
                  </div>

                   <div class="col-xs-6">

                      <div class="input-group" id="cantidadpagodiv">
                        <input type="number" id="cantidadpago" name="cantidadpago" class="form-control" placeholder="Cantidad a Pagar...." >
                    </div>

                      <div class="input-group" id="idtransacciondiv">
                        <input type="number" id="idtransaccion" name="idtransaccion" class="form-control" placeholder="ID Transaccion...." >
                    </div>
                  </div>
                </div>
          </div>

          <div class="box-footer">
            <a href="ventas"class="btn btn-danger pull-right">
         <i class="fa fa-times" aria-hidden="true"></i>

      Cancelar
        </a>
        <button type="submit" class="btn btn-success ">
         <i class="glyphicon glyphicon-saved"></i>
      Generar Venta</button>
          </div>



        </div>

      </div>
        </form>

        <?php

          $guardarVenta = new ControladorVentas();
          $guardarVenta -> ctrCrearVenta();
          
        ?>

    </div>
   
  </section>

</div>



<?php

require_once "conexion.php";

class ModeloCorte{

	/*=============================================
	MOSTRAR CORTE DE CAJA
	=============================================*/

	static public function mdlMostrarCortes($tabla, $item, $valor){

		if($item != null){
			$stmt = Conexion::conectar()->prepare("
				SELECT * FROM $tabla 
				WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		
		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	FUNCION DINAMICA DE PERSONALIZACION DE CONSULTAS
	=============================================*/	

	static public function mdlCortesMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){
		// $prueba="
		// 		SELECT * FROM $tabla WHERE ".$item ."=".$valor."
		// 										AND ".$item2." = ".$valor2." 
		// 										AND ".$item3." = ".$valor3." 
		// 										AND ".$item4." = ".$valor4." 
		// 				ORDER BY fechaalta DESC";
		// echo $prueba; exit();

		if($item != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4 
												AND $item5 = :$item5 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item5, $valor5, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();

					}else{
					// echo "4".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}

					}else{
					// echo "3".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}
				}else{

					// echo "2".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla 
															WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	RANGO FECHAS DE LOS CORTE
	=============================================*/	

	static public function mdlRangoFechasCorte($tabla, $fechaInicial, $fechaFinal){

		if($fechaInicial == null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();	


		}else if($fechaInicial == $fechaFinal){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha like '%$fechaFinal%'");

			$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$fechaActual = new DateTime();
			$fechaActual ->add(new DateInterval("P1D"));
			$fechaActualMasUno = $fechaActual->format("Y-m-d");

			$fechaFinal2 = new DateTime($fechaFinal);
			$fechaFinal2 ->add(new DateInterval("P1D"));
			$fechaFinalMasUno = $fechaFinal2->format("Y-m-d");

			if($fechaFinalMasUno == $fechaActualMasUno){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinalMasUno'");

			}else{


				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal'");

			}
		
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

	}

	/*=============================================
	REGISTRO DE NUEVO CORTE
	=============================================*/

	static public function mdlIngresarCorte($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(codigo, usuario_id, fechaalta, total, act, sucursal_id) VALUES (:codigo, :usuario_id, :fechaalta, :total,1,:sucursal_id)");

		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_INT);
		$stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
		$stmt->bindParam(":sucursal_id", $datos["sucursal_id"], PDO::PARAM_INT);

		if($stmt->execute()){
			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR VENTA
	=============================================*/

	static public function mdlEditarCorte($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  id_cliente = :id_cliente, id_vendedor = :id_vendedor, productos = :productos, impuesto = :impuesto, neto = :neto, total= :total, metodo_pago = :metodo_pago WHERE codigo = :codigo");

		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_INT);
		$stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
		$stmt->bindParam(":id_vendedor", $datos["id_vendedor"], PDO::PARAM_INT);
		$stmt->bindParam(":productos", $datos["productos"], PDO::PARAM_STR);
		$stmt->bindParam(":impuesto", $datos["impuesto"], PDO::PARAM_STR);
		$stmt->bindParam(":neto", $datos["neto"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
		$stmt->bindParam(":metodo_pago", $datos["metodo_pago"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR VENTA
	=============================================*/

	static public function mdlEliminarCorte($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}



	/*=============================================
	SUMAR EL TOTAL DE CORTE DE CAJA
	=============================================*/

	static public function mdlSumaTotalCorte($tabla){	

		// $stmt = Conexion::conectar()->prepare("SELECT SUM(neto) as total FROM $tabla");
		$stmt = Conexion::conectar()->prepare("SELECT SUM(total) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	OBTENER EL ULTIMO ID DEL CORTE GENERADO 
	=============================================*/

	static public function mdlMaxIDCorte($tabla, $item, $valor){	
		// echo "SELECT max(id) as idmax FROM ".$tabla." where ".$item."= ".$valor; exit();
		// $stmt = Conexion::conectar()->prepare("SELECT SUM(neto) as total FROM $tabla");
		$stmt = Conexion::conectar()->prepare("SELECT max(id) as idmax FROM $tabla where $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}


	
}
<?php

require_once "conexion.php";

class ModeloPago{

	/*=============================================
	MOSTRAR PAGO
	=============================================*/

	static public function mdlMostrarPagos($tabla, $item, $valor){

		if($item != null){
			$stmt = Conexion::conectar()->prepare("
				SELECT * FROM $tabla 
				WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		
		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	FUNCION DINAMICA DE PERSONALIZACION DE CONSULTAS
	=============================================*/	

	static public function mdlPagosMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){


		if($item != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4 
												AND $item5 = :$item5 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item5, $valor5, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();

					}else{
					// echo "4".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}

					}else{
					// echo "3".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}
				}else{

					// echo "2".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla 
															WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	REGISTRO DE VENTA
	=============================================*/

	static public function mdlAgregarPago($tabla, $datos){

// 		$stmt = "INSERT INTO $tabla(venta_id, venta_codigo, fechaalta, cantidad, total, datooperacion, tipopago_id, usuario_id, sucursal_id, act, resta) VALUES (".$datos['venta_id'].", ".$datos['venta_codigo'].", '".$datos['fechaalta']."', ".$datos['cantidad'].",".$datos['total'].", ".$datos['datooperacion'].", ".$datos['tipopago_id'].", ".$datos['usuario_id'].", ".$datos['sucursal_id'].", 1, ".$datos['resta'].")";
// echo $stmt; exit();
		$stmt = Conexion::conectar()->prepare("
			INSERT INTO $tabla(venta_id, venta_codigo, fechaalta, cantidad, total, datooperacion, tipopago_id, usuario_id, sucursal_id, act, resta) 
			VALUES (:venta_id, :venta_codigo, :fechaalta, :cantidad, :total, :datooperacion, :tipopago_id, :usuario_id, :sucursal_id, 1, :resta)");

			 $stmt->bindParam(":venta_id", $datos["venta_id"], PDO::PARAM_STR);
			 $stmt->bindParam(":venta_codigo", $datos["venta_codigo"], PDO::PARAM_STR);
			 $stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_STR);
			 $stmt->bindParam(":cantidad", $datos["cantidad"], PDO::PARAM_STR);
			 $stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
			 $stmt->bindParam(":datooperacion", $datos["datooperacion"], PDO::PARAM_STR);
			 $stmt->bindParam(":tipopago_id", $datos["tipopago_id"], PDO::PARAM_STR);
			 $stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_STR);
			 $stmt->bindParam(":sucursal_id", $datos["sucursal_id"], PDO::PARAM_STR);
			 $stmt->bindParam(":resta", $datos["resta"], PDO::PARAM_STR);


		if($stmt->execute()){

			return "ok";

		}else{

			return "ok";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR VENTA CON CORTE CORRESPONDIENTE
	=============================================*/

	static public function mdlActualizarPago($tabla, $item, $valor, $item1, $valor1, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		if($item1 != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :$item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 
												AND $item4 = :item4 
												AND $item5 = :item5");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":item4", $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":item5", $valor5, PDO::PARAM_STR);

					}else{
					// echo "4".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 ");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":item4", $valor4, PDO::PARAM_STR);

						}

					}else{
					// echo "3".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 ");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);

						}
				}else{

					// echo "2".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);

				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item = :item  
															WHERE $item1 = :item1 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);

		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item = :item");
			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
		}

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}

	/*=============================================
	EDITAR VENTA
	=============================================*/

	static public function mdlEditarPago($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  id_cliente = :id_cliente, id_vendedor = :id_vendedor, productos = :productos, impuesto = :impuesto, neto = :neto, total= :total, metodo_pago = :metodo_pago WHERE codigo = :codigo");

		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_INT);
		$stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
		$stmt->bindParam(":id_vendedor", $datos["id_vendedor"], PDO::PARAM_INT);
		$stmt->bindParam(":productos", $datos["productos"], PDO::PARAM_STR);
		$stmt->bindParam(":impuesto", $datos["impuesto"], PDO::PARAM_STR);
		$stmt->bindParam(":neto", $datos["neto"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
		$stmt->bindParam(":metodo_pago", $datos["metodo_pago"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR VENTA
	=============================================*/

	static public function mdlEliminarPago($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function mdlRangoFechasPago($tabla, $fechaInicial, $fechaFinal){

		if($fechaInicial == null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();	


		}else if($fechaInicial == $fechaFinal){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta like '%$fechaFinal%' ORDER BY fechaalta DESC");

			$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$fechaActual = new DateTime();
			$fechaActual ->add(new DateInterval("P1D"));
			$fechaActualMasUno = $fechaActual->format("Y-m-d");

			$fechaFinal2 = new DateTime($fechaFinal);
			$fechaFinal2 ->add(new DateInterval("P1D"));
			$fechaFinalMasUno = $fechaFinal2->format("Y-m-d");

			if($fechaFinalMasUno == $fechaActualMasUno){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinalMasUno' ORDER BY fechaalta DESC");

			}else{


				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY fechaalta DESC");

			}
		
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

	}

	/*=============================================
	SUMAR EL TOTAL DE PAGO
	=============================================*/

	static public function mdlSumaPago($tabla, $item, $valor){

		$prueba="SELECT SUM(total) as total FROM $tabla WHERE ".$item ."=".$valor;

		// echo $prueba; exit();

		if($item != null){
			$stmt = Conexion::conectar()->prepare($prueba);

			// $stmt = Conexion::conectar()->prepare("
			// 	SELECT SUM(cantidad) as total $tabla 
			// 	WHERE $item = :$item");

			// $stmt -> bindParam(":".$item, $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT SUM(cantidad) as total $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		
		$stmt -> close();

		$stmt = null;

	}

}
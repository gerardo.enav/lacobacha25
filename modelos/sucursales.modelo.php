<?php

require_once "conexion.php";

class ModeloSucursal{


	/*=============================================
	MOSTRAR CORTE DE CAJA
	=============================================*/

	static public function mdlMostrarSucursales($tabla, $item, $valor){
		// echo "
		// 		SELECT * FROM ".$tabla." WHERE ".$item." = ".$valor."
		// 		ORDER BY fechaalta DESC";


		if($item != null){
			$stmt = Conexion::conectar()->prepare("
				SELECT * FROM $tabla 
				WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		
		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	FUNCION DINAMICA DE PERSONALIZACION DE CONSULTAS
	=============================================*/	

	static public function mdlSucursalesMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){
		$prueba="
				SELECT * FROM $tabla WHERE ".$item ."=".$valor."
												AND ".$item2." = ".$valor2." 
												AND ".$item3." = ".$valor3." 
												AND ".$item4." = ".$valor4." 
						ORDER BY fechaalta DESC";
		echo $prueba; exit();

		if($item != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4 
												AND $item5 = :$item5 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item5, $valor5, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();

					}else{
					// echo "4".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}

					}else{
					// echo "3".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}
				}else{

					// echo "2".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla 
															WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	RANGO FECHAS DE LOS CORTE
	=============================================*/	

	static public function mdlRangoFechasSucursal($tabla, $fechaInicial, $fechaFinal){

		if($fechaInicial == null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();	


		}else if($fechaInicial == $fechaFinal){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha like '%$fechaFinal%'");

			$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$fechaActual = new DateTime();
			$fechaActual ->add(new DateInterval("P1D"));
			$fechaActualMasUno = $fechaActual->format("Y-m-d");

			$fechaFinal2 = new DateTime($fechaFinal);
			$fechaFinal2 ->add(new DateInterval("P1D"));
			$fechaFinalMasUno = $fechaFinal2->format("Y-m-d");

			if($fechaFinalMasUno == $fechaActualMasUno){

				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinalMasUno'");

			}else{


				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal'");

			}
		
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

	}

	/*=============================================
	REGISTRO DE NUEVO CORTE
	=============================================*/

	static public function mdlIngresarSucursal($tabla, $datos){
// 		echo "INSERT INTO ".$tabla."(nombre, direccion, telefono, email, fechaalta, usuario_id, colonia, cp, estado_id, municipio_id, act) VALUES (".$datos['nombre'].",". $datos['direccion'].",". $datos['telefono'].",". $datos['email'].",". $datos['fechaalta'].",". $datos['usuario_id'].",". $datos['colonia'].",". $datos['cp'].",". $datos['estado'].",". $datos['municipio'].",1)";
// exit();

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, direccion, telefono, email, fechaalta, usuario_id, , colonia, cp, estado_id, municipio_id, act) 
												VALUES (:nombre, :direccion, :telefono, :email, :fechaalta, :usuario_id, :colonia, :cp, :estadoid, :municipioid, 1)");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_INT);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_INT);
		$stmt->bindParam(":colonia", $datos["colonia"], PDO::PARAM_STR);
		$stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		$stmt->bindParam(":estadoid", $datos["estado"], PDO::PARAM_INT);
		$stmt->bindParam(":municipioid", $datos["municipio"], PDO::PARAM_INT);


		if($stmt->execute()){
			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}


	static public function mdlIngresarSucursal2($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, direccion,
			-- calle, numint, numext, colonia, cp, estado_id, municipio_id, 
			-- telefono, email, 
			-- horaapertura, horacierre, 
			fechaalta, usuario_id, act) VALUES (:nombre, direccion
			-- :calle, :numint, :numext, :colonia, :cp, :estado_id, :municipio_id, 
			-- :telefono, :email, 
			-- :horaapertura, :horacierre, 
			:fechaalta, :usuario_id, 1)");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_INT);
		// $stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_INT);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		// $stmt->bindParam(":calle", $datos["calle"], PDO::PARAM_STR);
		// $stmt->bindParam(":numint", $datos["numint"], PDO::PARAM_INT);
		// $stmt->bindParam(":numext", $datos["numext"], PDO::PARAM_INT);
		// $stmt->bindParam(":colonia", $datos["colonia"], PDO::PARAM_INT);
		// $stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		// $stmt->bindParam(":estado_id", $datos["estado_id"], PDO::PARAM_INT);
		// $stmt->bindParam(":municipio_id", $datos["municipio_id"], PDO::PARAM_INT);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_INT);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		// $stmt->bindParam(":horaapertura", $datos["horaapertura"], PDO::PARAM_INT);
		// $stmt->bindParam(":horacierre", $datos["horacierre"], PDO::PARAM_INT);


		if($stmt->execute()){
			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR VENTA
	=============================================*/

	static public function mdlEditarSucursal($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  id_cliente = :id_cliente, id_vendedor = :id_vendedor, productos = :productos, impuesto = :impuesto, neto = :neto, total= :total, metodo_pago = :metodo_pago WHERE codigo = :codigo");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_INT);
		$stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_INT);
		$stmt->bindParam(":calle", $datos["calle"], PDO::PARAM_STR);
		$stmt->bindParam(":numint", $datos["numint"], PDO::PARAM_INT);
		$stmt->bindParam(":numext", $datos["numext"], PDO::PARAM_INT);
		$stmt->bindParam(":colonia", $datos["colonia"], PDO::PARAM_INT);
		$stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		$stmt->bindParam(":estado_id", $datos["estado_id"], PDO::PARAM_INT);
		$stmt->bindParam(":municipio_id", $datos["municipio_id"], PDO::PARAM_INT);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_INT);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":horaapertura", $datos["horaapertura"], PDO::PARAM_INT);
		$stmt->bindParam(":horacierre", $datos["horacierre"], PDO::PARAM_INT);
		$stmt->bindParam(":act", $datos["act"], PDO::PARAM_INT);


		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR VENTA
	=============================================*/

	static public function mdlEliminarSucursal($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}


	
}